import {Layout, Spin} from 'antd';
import {Content, Footer, Header} from "./components";
import "./App.css";
import {useEffect, useState} from "react";

function App() {
    const [courses, setCourses] = useState();
    useEffect(() => {
        let url = "https://api.binance.com/api/v3/ticker/price?symbols=[\"USDTUAH\",\"EURUSDT\",\"BTCUSDT\",\"ETHUSDT\"]";
        fetch(url).then(res => {
            return res.json()
        }).then(data => {
            let result = [
                {'name': 'USD', 'price': parseFloat(data[3].price).toFixed(2)},
                {'name': 'EUR', 'price': parseFloat(data[2].price * data[3].price).toFixed(2)},
                {'name': 'BTC', 'price': parseFloat(data[0].price * data[3].price).toFixed(2)},
                {'name': 'ETH', 'price': parseFloat(data[1].price * data[3].price).toFixed(2)},
                {'name': 'UAH', 'price': 1},
            ]
            setCourses(result);
        })
    }, []);
    return (
        <Layout className="layout">
            <Header courses={courses}/>
            {courses ? <Content courses={courses}/> : <Spin/>}
            <Footer/>
        </Layout>
    );
}

export default App;
