import React from 'react';
import {Row, Col} from "antd";

function FormBlock(props) {
    return (
        <Row>
            <Col span={12}>
                <select onChange={(e) => {
                    props.handlerChangeVal(e.target.value, props.type)
                }}>
                    {props.courses && props.courses.map((item, index) => {
                        if(props.otherVal==index){
                            return '';
                        }
                        return (
                            <option value={index} key={index}>
                                {item.name}
                            </option>
                        )
                    })}
                </select>
            </Col>

        </Row>
    );
}

export default FormBlock;