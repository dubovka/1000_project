import React from 'react';
import { Layout} from 'antd';

function Footer(props) {
    const { Footer } = Layout;
    return (
        <Footer style={{textAlign: 'center'}}>
            Тестовое задание 1001
            Telegram:<a target="_blank" href="https://t.me/gradoded">@gradoded</a>
        </Footer>
    );
}

export default Footer;