import React, {useEffect, useState} from 'react';
import {Col, Layout, Row} from 'antd';
import styles from "./style.module.css";
import {FormBlock} from "../index";

function Content(props) {
    const {Content} = Layout;
    const [count, setCount] = useState(0)

    const [leftVal, setLeftVal] = useState(0)
    const [rightVal, setRightVal] = useState(1)
    const [result, setResult] = useState(0);


    const handlerChangeVal = (newValue, type) => {
        if (type == 'left') {
            setLeftVal(newValue);
        } else if (type == 'right') {
            setRightVal(newValue);
        }
    }
    useEffect(() => {
        let from = count * props.courses[leftVal].price;
        console.log('from', from);
        let to = props.courses[rightVal].price;
        console.log('to', to);
        let result = parseFloat(from / to).toFixed(2);
        setResult(result)
    }, [count, rightVal, leftVal]);

    return (
        <Content className={styles.contentContainer}>
            <div className={styles.content}>
                <h2>Лучший калькулятор</h2>
                <Row>
                    <Col span={6}>
                        Количество
                        <div>
                            <input value={count} onChange={e => {
                                setCount(e.target.value)
                            }}/>
                        </div>

                    </Col>
                    <Col span={6}>
                        Из <FormBlock courses={props.courses} type="left" otherVal={rightVal}
                                      handlerChangeVal={handlerChangeVal}/>
                    </Col>
                    <Col span={6}>
                        В <FormBlock courses={props.courses} type="right" otherVal={leftVal}
                                     handlerChangeVal={handlerChangeVal}/>
                    </Col>
                    <Col span={6}>
                        Получите:
                        <div>{result} {props.courses[rightVal].name}</div>
                    </Col>
                </Row>
            </div>
        </Content>
    );
}

export default Content;