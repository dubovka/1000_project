import React from 'react';
import {Layout, Spin} from "antd";
import styles from "./style.module.css";

function Header(props) {
    const {Header} = Layout;
    return (
        <Header className={styles.content}>
            USD: {props.courses ? (props.courses[0].price) : <Spin />}
        </Header>
    );
}

export default Header;